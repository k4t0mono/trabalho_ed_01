/*
  Trabalho de Estrutura de Dados
  Reconhecedor da linguagem L
  Copyright (c) 2016 Ábner Neves, Breno Gomes
*/

#include <iostream>
using namespace std;

class Node {
  protected:
    char value;
    Node* next;

  public:
    Node(char v = '\0');
    ~Node();

  friend class Stack;
};

class Stack {
  protected:
    int size;
    Node* header;
    Node* tail;

  public:
    Stack();
    ~Stack();

    void push(char v);
    char spy();
    char drop();

    int getSize();
};

// Node Class
Node::Node(char v) {
  value = v;
  next = nullptr;
}

Node::~Node() {
  value = 0;
  next = nullptr;
}

// Stack Class
Stack::Stack() {
  size = 0;
  header = tail = nullptr;
}

Stack::~Stack() {
  if(size == 0) {
    return;
  }

  size = 0;
  Node* aux = header->next;
  delete header;

  while(aux != nullptr) {
    header = aux;
    aux = aux->next;
    delete header;
  }

  header = tail = nullptr;
  delete aux;
}

void Stack::push(char v) {
  Node* nd = new Node(v);

  if(!size) {
    ++size;
    header = tail = nd;
    return;
  }

  ++size;
  nd->next = header;
  header = nd;
}

char Stack::spy() {
  if(!size) {
    throw "Empty list";
  }

  return header->value;
}

char Stack::drop() {
  // Verificar se a lista não é nula
  if(!size) {
    throw "Empty list";
  }

  // Declaração das variaveis auziliares
  char v = header->value;
  Node* tmp = header;

  // Se a o tamanho for 1
  if(size == 1) {
    header = nullptr;
  } else {
    header = header->next;
  }

  --size;
  delete tmp;
  return v;
}

int Stack::getSize() {
  return size;
}

bool Recognizer(Stack* st) {
  // Ler o primeiro caractere
  char c = st->drop();
  // Se não estiver vazio e for 'B'
  if((st->getSize()) and (c == 'B')) {
    // Desempilhar todos 'B'
    while(st->getSize() and c == 'B') {
      c = st->drop();
    }
    // Se `c == 'A'`
    if(c == 'A') {
      // Desempilhar todos 'A'
      while(st->getSize() and c == 'A') {
        c = st->drop();
      }

      // Se a ultima letra não for 'A'
      if(c != 'A') {
        // Retornar falso
        return false;

      // Caso contrario
      } else {
        // Retornar true
        return true;
      }

    // Se `c == 'C'`
    } else if(c == 'C') {
      // Desempilhar todos 'C'
      while(st->getSize() and (c == 'C')) {
        c = st->drop();
      }
      // Se não estiver vazio e a última letra for 'A'
      if(st->getSize() and (c == 'A')) {
        // Desempilhar todos 'A'
        while(st->getSize() and c == 'A') {
          c = st->drop();
        }

        // Se a ultima letra não for 'A'
        if(c != 'A') {
          // Retornar falso
          return false;

        // Caso contrario
        } else {
          // Retornar true
          return true;
        }

      // Caso contrario
      } else {
        // Retornar false
        return false;
      }

    // Caso contrario
    } else {
      // Retornar falso
      return false;
    }
  // Caso contrario
  } else {
    // Retornar false
    return false;
  }
}

inline bool Belongs(char c) {
  if((c == 'A') or (c == 'B') or (c == 'C')) {
    return true;
  }

  return false;
}

int main() {
  Stack st;

  cout << "Reconhecedor da lingua L\n\n";

  cout << "Alfabeto S da lingua L: S = {A, B, C}\n"
       << "Padrao da Lingua L: L = {A^n C^m B^n | n > 0, m >= 0}\n"
       << "Insira a palavra a ser verificada, digite qualquer outro"
       << " caractere para sair da leitura.\n\n";

  char c;
  cin >> c;
  c = toupper(c);
  while((c == 'A') or (c == 'B') or (c == 'C')) {
    st.push(c);
    cin >> c;
    c = toupper(c);
  }

  cout << endl;

  if(st.getSize() and Recognizer(&st)) {
    cout << "Essa palavra faz parte da lingua L.\n";
  } else {
    cout << "Essa palavra nao eh valida.\n";
  }

  return 0;
}
