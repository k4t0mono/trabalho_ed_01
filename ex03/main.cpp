/*
  Trabalho de Estrutura de Dados

  Copyright (c) 2016 Ábner Neves, Breno Gomes
  main.cpp - Arquivo principal
*/

#include <iostream>
#include "aux.hpp"
using namespace std;

bool Recognizer(Stack* st) {
  // Ler o primeiro caractere
  char c = st->drop();
  // Se não estiver vazio e for 'B'
  if((st->getSize()) and (c == 'B')) {
    // Desempilhar todos 'B'
    while(st->getSize() and c == 'B') {
      c = st->drop();
    }
    // Se `c == 'A'`
    if(c == 'A') {
      // Desempilhar todos 'A'
      while(st->getSize() and c == 'A') {
        c = st->drop();
      }

      // Se a ultima letra não for 'A'
      if(c != 'A') {
        // Retornar falso
        return false;

      // Caso contrario
      } else {
        // Retornar true
        return true;
      }

    // Se `c == 'C'`
    } else if(c == 'C') {
      // Desempilhar todos 'C'
      while(st->getSize() and (c == 'C')) {
        c = st->drop();
      }
      // Se não estiver vazio e a última letra for 'A'
      if(st->getSize() and (c == 'A')) {
        // Desempilhar todos 'A'
        while(st->getSize() and c == 'A') {
          c = st->drop();
        }

        // Se a ultima letra não for 'A'
        if(c != 'A') {
          // Retornar falso
          return false;

        // Caso contrario
        } else {
          // Retornar true
          return true;
        }

      // Caso contrario
      } else {
        // Retornar false
        return false;
      }

    // Caso contrario
    } else {
      // Retornar falso
      return false;
    }
  // Caso contrario
  } else {
    // Retornar false
    return false;
  }
}

int main() {
  Stack st;

  cout << "Reconhecedor da lingua L\n\n";

  cout << "Alfabeto S da lingua L: S = {A, B, C}\n";
  cout << "Insira a palavra a ser verificada, digite qualquer outro"
       << " caractere para sair da leitura.\n\n";

  char c;
  cin >> c;
  c = toupper(c);
  while((c == 'A') or (c == 'B') or (c == 'C')) {
    st.push(c);
    cin >> c;
    c = toupper(c);
  }

  cout << endl;

  if(Recognizer(&st)) {
    cout << "Essa palavra faz parte da lingua L.\n";
  } else {
    cout << "Essa palavra nao eh valida.\n";
  }

  return 0;
}
