/*
  Trabalho de Estrutura de Dados

  Copyright 2016 Ábner Neves, Breno Gomes

*/

#include "aux.hpp"
using namespace std;

// Node Class
Node::Node(char v) {
  value = v;
  next = nullptr;
}

Node::~Node() {
  value = 0;
  next = nullptr;
}

// Stack Class
Stack::Stack() {
  size = 0;
  header = tail = nullptr;
}

Stack::~Stack() {
  if(size == 0) {
    return;
  }

  size = 0;
  Node* aux = header->next;
  delete header;

  while(aux != nullptr) {
    header = aux;
    aux = aux->next;
    delete header;
  }

  header = tail = nullptr;
  delete aux;
}

void Stack::push(char v) {
  Node* nd = new Node(v);

  if(!size) {
    ++size;
    header = tail = nd;
    return;
  }

  ++size;
  nd->next = header;
  header = nd;
}

char Stack::spy() {
  if(!size) {
    throw "Empty list";
  }

  return header->value;
}

char Stack::drop() {
  // Verificar se a lista não é nula
  if(!size) {
    throw "Empty list";
  }

  // Declaração das variaveis auziliares
  char v = header->value;
  Node* tmp = header;

  // Se a o tamanho for 1
  if(size == 1) {
    header = nullptr;
  } else {
    header = header->next;
  }

  --size;
  delete tmp;
  return v;
}

int Stack::getSize() {
  return size;
}
