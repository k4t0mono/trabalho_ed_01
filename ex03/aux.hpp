/*
  Trabalho de Estrutura de Dados

  Copyright 2016 Ábner Neves, Breno Gomes

*/

#ifndef AUX_HPP
#define AUX_HPP

class Node {
  protected:
    char value;
    Node* next;

  public:
    Node(char v = '\0');
    ~Node();

  friend class Stack;
};

class Stack {
  protected:
    int size;
    Node* header;
    Node* tail;

  public:
    Stack();
    ~Stack();

    void push(char v);
    char spy();
    char drop();

    int getSize();
};

#endif
