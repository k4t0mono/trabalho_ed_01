/*
  Trabalho de Estrutura de Dados

  Copyright 2016 Ábner Neves, Breno Gomes
  aux.hpp - Funções auxiliares, declaração
*/

#ifndef AUX_HPP
#define AUX_HPP

struct Filme {
  bool valido;
  char nome[21];
  int ano;
  int duracao;
  char genero[13];
  int imdb;
  int tomatoes;

  Filme& operator=(const Filme& a);
  friend std::istream& operator>>(std::istream& is, Filme& fl);
  friend std::ostream& operator<<(std::ostream& out, Filme& fl);
};

class Catalogo {
private:
  Filme* filmes;
  bool* usados;
  int cap;
  int tam;
  std::fstream* file;

public:
  Catalogo(int c = 10);
  ~Catalogo();

  void insert(Filme* fl);
  void insertOnFile(Filme* fl);
  void remove(int pos);
  void removeOnFile(int pos);
  void read(bool* status, int* n);
  void write();
  void list();
  void resize();

  void sortOlder();
  void sortNewer();
  void sortAZ();
  void sortZA();

  void resetFile();

  friend struct Menu;
};

void CriarArquivo();

struct Menu {
  Catalogo* cat;
  bool loaded;
  int tam;

  Menu(Catalogo* cat);

  void initialize();
  void read();
  void insert();
  void remove();
  void sort();
};


#endif
