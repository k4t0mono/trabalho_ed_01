/*
  Trabalho de Estrutura de Dados
  Copyright 2016 Ábner Neves, Breno Gomes
  main.cpp - Arquivo principal
*/

#include <iostream>
#include <cstring>
using namespace std;

class Decimal{
    private:
        int left;
        int right;
        int precision;

    public:
        //construtor para Decimal
        Decimal(int l, int r, int p);

        //sobrecarga de operadores
        friend ostream& operator<< (ostream& os, const Decimal& n);
        Decimal& operator+ (const Decimal& n);
        Decimal& operator* (const Decimal& n);

        //negar números
        void negar();

        //retorna uma string com a quantidade de zeros necessária para completar a precisão
        string zeros(int r, int p) const;

        void somarParteDecimal(const Decimal& n2, Decimal& resultado, int* right1, int* right2);

        //getters
        int getLeft() const { return this->left; }
        int getRight() const { return this->right; }
        int getPrecision() const { return this->precision; }

        //setters
        void setRight(int a) { this->right = a; };
        void setLeft(int a) { this->left = a; } ;
        void setPrecision(int a) { this->precision = a; };
};

Decimal::Decimal(int l, int r, int p){
    this->left = l;
    this->right = r;
    this->precision = p;

}

int qntdDigitos(int n){
    int qntd = 0;
    while (n){
        n /= 10;
        ++qntd;
    }
    return qntd;
}

string Decimal::zeros(int r, int p) const{
    unsigned int n = qntdDigitos(r);
    string zeros = "";

    //salva na string zeros a quantidade de zeros que falta para completar a precisão
    while (p-n > 0) {
        --p;
        zeros += '0';
    }
    return zeros;
}

void igualarCasasDecimais(const Decimal& n1, const Decimal& n2, Decimal& resultado, int* right1, int* right2){
        const int n1Precision = n1.getPrecision();
        const int n2Precision = n2.getPrecision();
        const int n1Right = n1.getRight();
        const int n2Right = n2.getRight();

        //atribui ao resultado a maior precisão entre os dois números a serem somados
        resultado.setPrecision(n1Precision);

        //calcula a diferença entre as precisões para, no próximo passo, igualar as casas decimais a serem somadas
        int diferenca = n1Precision - n2Precision;

        *right1 = n2Right;

        //multiplica por 10 a parte direita que tem menos casas até que se iguale à maior
        while (diferenca){
            *right1 *= 10;
            --diferenca;
        }
        *right2 = n1Right;
}

void Decimal::somarParteDecimal(const Decimal& n2, Decimal& resultado, int* right1, int* right2){
        const int n1Precision = this->getPrecision();
        const int n2Precision = n2.getPrecision();
        const int n1RightLength = qntdDigitos(this->getRight());
        const int n2RightLength = qntdDigitos(n2.getRight());

    if (n1Precision > n2Precision){

        igualarCasasDecimais(*this, n2, resultado, right1, right2);
    }
    else{
        if (n2Precision > n1Precision){
            igualarCasasDecimais(n2, *this, resultado, right1, right2);
        }
        else{
            if (n1RightLength > n2RightLength){
                igualarCasasDecimais(*this, n2, resultado, right1, right2);
            }
            else{
                igualarCasasDecimais(n2, *this, resultado, right1, right2);
            }
        }
    }

    int somaRight;

    //se algum dos números for negativo, vai subtrair as partes decimais ao invés de somar
    if (this->getLeft() < 0){
        if (n2.getLeft() < 0){
            somaRight = *right1 + *right2;
        }
        else{
            somaRight = *right2 - *right1;
        }
    }
    else{
        if (n2.getLeft() < 0){
            somaRight = *right1 - *right2;
        }
        else{
            somaRight = *right1 + *right2;
        }
    }

    if (somaRight < 0){
        somaRight *= -1;
    }

    //"vai um"
    if (qntdDigitos(somaRight) > resultado.getPrecision()){
        int parteInteira = 1;
        int n = qntdDigitos(somaRight) - 1;
        while (n){
            parteInteira *= 10;
            --n;
        }

        somaRight -= parteInteira;
        int novoLeft = resultado.getLeft() + 1;
        resultado.setLeft(novoLeft);
    }

    resultado.setRight(somaRight);
}

int tirarVirgula(int fLeft, int fRight, int fSize){
    int f = fLeft;
    while (fSize){
    f *= 10;
    --fSize;
    }

    f += fRight;

    return f;
}

ostream& operator<< (ostream& os, const Decimal& n){
    os << n.getLeft() << '.' << n.zeros(n.getRight(), n.getPrecision()) << n.getRight();
    return os;
}

Decimal& Decimal::operator+ (const Decimal& d){
    Decimal* resultado = new Decimal(0, 0, 0);

    //soma as partes esquerdas dos dois números
    int somaLeft = d.getLeft() + this->getLeft();
    resultado->setLeft(somaLeft);

    int right1, right2;

    this->somarParteDecimal(d, *resultado, &right1, &right2);

    return *resultado;
}

Decimal& Decimal::operator* (const Decimal& n2){
    Decimal* resultado = new Decimal(0, 0, 0);

    int f1Left = this->getLeft();
    int f2Left = n2.getLeft();
    int f1Right = this->getRight();
    int f2Right = n2.getRight();
    int f1Precision = this->getPrecision();
    int f2Precision = n2.getPrecision();
    int rPrecision = f1Precision + f2Precision;
    resultado->setPrecision(rPrecision);

    if (f1Left < 0){
        f1Left *= -1;
    }
    if (f2Left < 0){
        f2Left *= -1;
    }

    //transforma os fatores em inteiros
    long int f1 = tirarVirgula(f1Left, f1Right, f1Precision);
    long int f2 = tirarVirgula(f2Left, f2Right, f2Precision);

    long int produto = f1 * f2;

    //acha o divisor que deixará o produto com a quantidade certa de casas decimais e inteiras, "recolocando a vírgula""
    int p = rPrecision;
    int d = 1;
    while (p){
        d *= 10;
        --p;
    }

    //encontra a parte decimal do produto
    int rRight = produto % d;
    resultado->setRight(rRight);

    //encontra a parte inteira do produto
    int rLeft = (produto - rRight)/d;
    resultado->setLeft(rLeft);

    if ((this->getLeft() < 0 and n2.getLeft() >= 0) or (this->getLeft() >= 0 and n2.getLeft() < 0)){
        resultado->setLeft(rLeft * -1);
    }

    return *resultado;
}

void Decimal::negar(){
    int left;
    left = this->getLeft();
    right = this->getRight();

    left *= -1;
    this->setLeft(left);
}

int main(){
    int l, r, p;
    char operacao;
    cout << "Digite 's' para somar, 'm' para multiplicar e 'n' para negar." << endl;
    cin >> operacao;
    switch(operacao){
        case 's':{
            cout << "Caso um dos números seja negativo, apenas a parte inteira deve conter o sinal de menos" << endl
                 << "Parte inteira do primeiro número: ";
            cin >> l;
            cout << "Parte decimal do primeiro número: ";
            cin >> r;
            cout << "Precisão do primeiro número: ";
            cin >> p;
            Decimal* n1 = new Decimal(l, r, p);
            cout << "Primeiro número: " << *n1 << endl;

            cout << "Parte inteira do segundo número: ";
            cin >> l;
            cout << "Parte decimal do segundo número: ";
            cin >> r;
            cout << "Precisão do segundo número: ";
            cin >> p;
            Decimal* n2 = new Decimal(l, r, p);
            cout << "Segundo número: " << *n2 << endl;

            cout << "A soma dos dois é " << *n1+*n2 << endl;
            delete(n1);
            delete(n2);
        break;}

        case 'm':{
            cout << "Caso um dos números seja negativo, apenas a parte inteira deve conter o sinal de menos" << endl
                 << "Parte inteira do primeiro número: ";
            cin >> l;
            cout << "Parte decimal do primeiro número: ";
            cin >> r;
            cout << "Precisão do primeiro número: ";
            cin >> p;
            Decimal* n1 = new Decimal(l, r, p);
            cout << "Primeiro número: " << *n1 << endl;

            cout << "Parte inteira do segundo número: ";
            cin >> l;
            cout << "Parte decimal do segundo número: ";
            cin >> r;
            cout << "Precisão do segundo número: ";
            cin >> p;
            Decimal* n2 = new Decimal(l, r, p);
            cout << "Segundo número: " << *n2 << endl;

            cout << "A multiplicação dos dois é " << *n1**n2 << endl;
            delete(n1);
            delete(n2);
        break;}

        case 'n':{
            cout << "Caso o número seja negativo, apenas a parte inteira deve conter o sinal de menos" << endl
                 << "Parte inteira do número: ";
            cin >> l;
            cout << "Parte decimal do número: ";
            cin >> r;
            cout << "Precisão do número: ";
            cin >> p;
            Decimal* n = new Decimal(l, r, p);
            cout << "Número: " << *n << endl;
            n->negar();
            cout << "Número negado: " << *n << endl;
        break;}
        default:
            cout << "Digite uma opção válida" << endl;
    }
    return 0;
}
