/*
  Trabalho de Estrutura de Dados

  Copyright 2016 Ábner Neves, Breno Gomes
  main.cpp - Arquivo principal
*/

#include <iostream>
#include "aux.hpp"
using namespace std;

int main() {
  Catalogo cat;
  Menu(&cat).initialize();

  return 0;
}
