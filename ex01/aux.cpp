/*
  Trabalho de Estrutura de Dados

  Copyright 2016 Ábner Neves, Breno Gomes
  aux.hpp - Funções auxiliares, implementação
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "aux.hpp"
using namespace std;

// ==================================================================
//  Struct Filme
// ==================================================================

// Operator=
Filme& Filme::operator=(const Filme& a) {
  // Igualar cada atributo
  strcpy(nome, a.nome);
  ano = a.ano;
  duracao = a.duracao;
  strcpy(genero, a.genero);
  imdb = a.imdb;
  tomatoes = a.tomatoes;
  valido = true;

  return *this;
}

// Operator>>
istream& operator>>(istream& is, Filme& fl) {
  cout << "% Nome(20): ";
  is.clear();
  is.ignore();
  is.getline(fl.nome, 21);

  cout << "% Ano: ";
  is >> fl.ano;

  cout << "% Duração: ";
  is >> fl.duracao;

  cout << "% Gênero(12): ";
  is.ignore();
  is.getline(fl.genero, 13);

  cout << "% Nota no IMDB: ";
  float aux;
  is >> aux;
  fl.imdb = int(aux);

  cout << "% Nota no Rotten Tomatoes: ";
  is >> aux;
  fl.tomatoes = int(aux);

  return is;
}

// Operator<<
ostream& operator<<(ostream& out, Filme& fl) {
  out << fl.nome << endl;
  out << "  Ano:      " << fl.ano << endl;
  out << "  Duração:  " << fl.duracao << endl;
  out << "  Gênero:   " << fl.genero << endl;
  out << "  IMDB:     " << fl.imdb << endl;
  out << "  Tomatoes: " << fl.tomatoes << endl;

  return out;
}

// ==================================================================
//  Class Catalogo
// ==================================================================

// Construtor
Catalogo::Catalogo(int c) {
  cap = c;
  tam = 0;
  filmes = new Filme[cap];
  usados = new bool[cap];

  CriarArquivo();
  fstream* fl = new fstream;
  fl->open("catalogo.dat", ios::binary | ios::in | ios::out);
  file = fl;
}

// Destrutor
Catalogo::~Catalogo() {
  delete[] filmes;
  delete[] usados;
  cap = tam = 0;
  file->close();
  delete file;
}

// Inserir Filme
void Catalogo::insert(Filme* fl){
  // Se o vetor não estiver cheio
  if(cap != tam) {
    ++tam;
    // Achar a proxima posição vazia
    int pos = 0;
    while(usados[pos]) {
      ++pos;
    }
    // Indicar que a posião 'pos' está ocupada
    usados[pos] = true;
    // Indicar que o elemento é valido
    fl->valido = true;
    // Igualar o elemento da posição 'pos' à 'fl'
    filmes[pos] = *fl;
    return;
  // Caso contrário
  } else {
    // Redimencionar catalogo
    resize();
    // Inserir elemento
    insert(fl);
  }
}

// Inserir no arquivo
void Catalogo::insertOnFile(Filme* fl) {
  fl->valido = true;
  // Se o arquivo estiver vazio ou conter um filme invalido
  if(!tam) {
    // Escrever o filme
    file->write((const char*)(fl), sizeof(Filme));
  } else {
    // Achar o primeiro filme invalido
    Filme aux;
    aux.valido = true;
    int n = -1;
    // Enquanto e não acabar o arquivo
    while(!file->eof() and aux.valido) {
      file->read((char*)(&aux), sizeof(Filme));
      ++n;
    }
    // Se não chegou no fim do arquivo
    if(!file->eof()) {
      // Sobrescreverá um filme inválido
      int p = file->tellg();
      p -= sizeof(Filme);
      file->seekp(p);
      file->write((char*)(fl), sizeof(Filme));
      // Caso contrario
    } else {
      // O filme será escrito no fim do arquivo
      file->clear();
      int p = sizeof(Filme)*n;
      file->seekp(p);
      file->write((char*)(fl), sizeof(Filme));
    }
  }

  insert(fl);
  resetFile();
}

// Remover filme
void Catalogo::remove(int pos) {
  --tam;
  usados[pos] = filmes[pos].valido = false;
}

// Remover no arquivo
void Catalogo::removeOnFile(int pos) {
  // Remover da memoria
  remove(pos);
  // Achar a posição a ser escrita
  int p = sizeof(Filme)*pos;
  // Posicionar ponteriro
  file->seekp(p);
  // Marcar como invalido
  bool a = false;
  file->write((const char*)(&a), sizeof(bool));
  // Resetar flags
  resetFile();
}

// Ler do arquivo
void Catalogo::read(bool* status, int* n) {
  // Enquanto não chegar no fim do arquivo
  Filme* aux = new Filme;
  while(!file->eof()) {
    // Ler filme
    file->read((char*)(aux), sizeof(Filme));
    // Se for valido e não estiver no fim do arquivo
    if(aux->valido and !file->eof()) {
      // Inserir no vetor
      insert(aux);
    }
  }
  resetFile();
  delete aux;

  // Se o arquivo estiver vazio
  if(tam) {
    // Mudar status para true
    *status = true;
  // Caso contrario
  } else {
    // Mudar status para false
    *status = false;
  }
  // Retornar o numero de filmes lidos
  *n = tam;
}

// Escrever no arquivo
void Catalogo::write() {
  // Escrever vetor de filmes
  for(int i = 0; i < cap; ++i) {
    if(filmes[i].valido) {
      file->write((const char*)(&filmes[i]), sizeof(Filme));
    }
  }
}

void Catalogo::list() {
  if(tam) {
    cout << "*** Listagem do Catálogo ***\n\n";

    for(int i = 0; i < cap; ++i) {
      if(usados[i]) {
        cout << filmes[i] << endl;
      }
    }

    cout << "*** ***\n";
  } else {
    cout << "% Catalogo vazio.\n"
         << "% Nada para exibir.\n";
  }
}

// Resize
void Catalogo::resize() {
  // Aumentar capacidade
  cap = int(cap*1.5) + 1;

  // Criar novo vetor de filmes
  Filme* novoF = new Filme[cap];
  // Criar novo vetor de usados
  bool* novoU = new bool[cap];

  // Copiar para os novos vetores
  for(int i = 0; i < tam; ++i) {
    novoF[i] = filmes[i];
    novoU[i] = usados[i];
  }

  // Desalocar vetores antigos
  delete[] filmes;
  delete[] usados;

  // Apontar para os novos vetores
  filmes = novoF;
  usados = novoU;

  // Apontar auxiliares para null
  novoF = nullptr;
  novoU = nullptr;

  // Desalocar auxiliares
  delete[] novoF;
  delete[] novoU;
}

// Sort by Year - Older first - Selection Sort
void Catalogo::sortOlder() {
  // Para cada filme no vetor
  for(int i = 0; i < cap; ++i) {
    // Se o filme for valido
    if(usados[i]) {
      int iMenor = i;
      // Achar o menor
      for(int j = i+1; j < cap; ++j) {
        if(usados[j] and (filmes[j].ano < filmes[iMenor].ano)) {
          iMenor = j;
        }
      }
      // Se não for ele mesmo
      if(iMenor != i) {
        // Trocar de posição
        Filme tmp = filmes[iMenor];
        filmes[iMenor] = filmes[i];
        filmes[i] = tmp;
      }
    }
  }
  write();
}

// Sort by Year - Newer first - Selection Sort
void Catalogo::sortNewer() {
  // Para cada filme no vetor
  for(int i = 0; i < cap; ++i) {
    // Se o filme for valido
    if(usados[i]) {
      int iMaior = i;
      // Achar o maior
      for(int j = i+1; j < cap; ++j) {
        if(usados[j] and (filmes[j].ano > filmes[iMaior].ano)) {
          iMaior = j;
        }
      }
      // Se não for ele mesmo
      if(iMaior != i) {
        // Trocar de posição
        Filme tmp = filmes[iMaior];
        filmes[iMaior] = filmes[i];
        filmes[i] = tmp;
      }
    }
  }
  write();
}

// Sort by Name - A first - Selection Sort
void Catalogo::sortAZ() {
  // Para cada filme no vetor
  for(int i = 0; i < cap; ++i) {
    // Se o filme for valido
    if(usados[i]) {
      int iMenor = i;
      // Achar o menor
      for(int j = i+1; j < cap; ++j) {
        if(usados[j] and (filmes[j].nome > filmes[iMenor].nome)) {
          iMenor = j;
        }
      }
      // Se não for ele mesmo
      if(iMenor != i) {
        // Trocar de posição
        Filme tmp = filmes[iMenor];
        filmes[iMenor] = filmes[i];
        filmes[i] = tmp;
      }
    }
  }
  write();
}

// Sort by Name - Z first - Selection Sort
void Catalogo::sortZA() {
  // Para cada filme no vetor
  for(int i = 0; i < cap; ++i) {
    // Se o filme for valido
    if(usados[i]) {
      int iMaior = i;
      // Achar o maior
      for(int j = i+1; j < cap; ++j) {
        if(usados[j] and (filmes[j].ano < filmes[iMaior].ano)) {
          iMaior = j;
        }
      }
      // Se não for ele mesmo
      if(iMaior != i) {
        // Trocar de posição
        Filme tmp = filmes[iMaior];
        filmes[iMaior] = filmes[i];
        filmes[i] = tmp;
      }
    }
  }
  write();
}

// ==================================================================
//  Funções Axiliares
// ==================================================================

void Catalogo::resetFile() {
  // Zerar flags
  file->clear();
  // Zerar ponteriros
  file->seekp(0);
  file->seekg(0);
}

void CriarArquivo() {
  ifstream a("catalogo.dat", ios::binary);
  if(!a) {
    ofstream b("catalogo.dat", ios::binary);
    char a = '\0';
    b.write(&a, sizeof(char));
    b.close();
  }
  a.close();
}

// ==================================================================
//  Menu
// ==================================================================

Menu::Menu(Catalogo* c) {
  cat = c;
  loaded = false;
}

void Menu::initialize() {
  cout << "*** Catalogo de Filmes ***\n\n";
  char op;

  read();

  do {
    cout << "\nOpções\n"
         << " (L)istar catálogo\n"
         << " (I)nserir filme\n"
         << " (R)emover filme\n"
         << " (O)rdenar\n"
         << " (B)uscar\n"
         << " (S)air\n";

    cout << endl;
    cout << "> ";
    cin >> op;
    cout << endl;

    switch(op) {
      case 'L': cat->list(); break;
      case 'I': insert(); break;
      case 'R': remove(); break;
      case 'O': sort(); break;
      // case 'B': cat->; break;
    }
  } while(op != 'S');
}

void Menu::read() {
  bool status;
  int n;
  cat->read(&status, &n);
  tam = n;

  if(status) {
    cout << "% Catalogo lido com sucesso.\n"
         << "% Foram lidos " << n << " filmes.\n";
  } else {
    cout << "% O arquivo estava vazio.\n"
         << "% Nenhum filme foi lido.\n";
  }
}

void Menu::insert() {
  Filme tmp;
  cin >> tmp;
  cout << endl;
  cat->insertOnFile(&tmp);
  cout << "% Filme inserido com sucesso\n";
}

void Menu::remove() {
  if(cat->tam) {
    char op;
    do {
      cout << "% Qual fime deseja excluir?\n";

      for(int i = 0; i < tam; ++i) {
        cout << " (" << i << ") " << cat->filmes[i].nome << endl;
      }

      cout << "\n> ";
      int n;
      cin >> n;
      cout << endl;

      cout << "Tem certeza? (S)im ou (N)ão\n";
      cout << "\n> ";
      cin >> op;

      if(op == 'S') {
        cat->removeOnFile(n);
      }
    } while(op == 'N');
  } else {
    cout << "% Catalogo vazio.\n"
         << "% Sem filmes para excluir\n";
  }
}

void Menu::sort() {
  cout << "% Como você deseja ordenar?\n"
       << "% Ao ordenar, as modificações serão salvas no aqruivo\n"
       << "  (1) Por ano - mais velhos primeiro\n"
       << "  (2) Por ano - mais novos primeiro\n"
       << "  (3) Por nome - A primero\n"
       << "  (4) Por nome - Z primeiro\n"
       << "  (0) Cancelar\n"
       << "\n"
       << "> ";

  int op;
  cin >> op;

  switch (op) {
    case 0: return; break;
    case 1: cat->sortOlder(); break;
    case 2: cat->sortNewer(); break;
    case 3: cat->sortAZ(); break;
    case 4: cat->sortZA(); break;
    default:
      cout << "% Opção inválida.\n"
           << "% Cancelando.\n";
      return;
      break;
  }
}
